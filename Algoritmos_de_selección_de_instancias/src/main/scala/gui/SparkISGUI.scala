package gui

/**
 *
 * Clase lanzadora de la interfaz gráfica.
 *
 * @author Alejandro González Rogel
 * @version 1.0.0
 */
object SparkISGUI extends App {

  /**
   * Ventana principal de la aplicación.
   */
  val ui = new UI
  ui.visible = true

}
