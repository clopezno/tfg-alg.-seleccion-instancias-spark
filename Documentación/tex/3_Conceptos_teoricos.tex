\capitulo{3}{Conceptos teóricos}

% En aquellos proyectos que necesiten para su comprensión y desarrollo de unos conceptos teóricos de una determinada materia o de un determinado dominio de conocimiento, debe existir un apartado que sintetice dichos conceptos.

\section{Minería de datos}\label{DefMineria}

Es el proceso mediante el cual se pretende extraer conocimiento de un conjunto de datos que, sin ser tratados o analizados previamente, no nos proporcionan información útil \cite{DataMiningConcepts}.

Se trata de un término que podría generar confusión con el de KDD (\textit{Knowledge Discovery from Data} \cite{fayyad1996data}), siendo en ocasiones tratado como un mero sinónimo de este término (que apareció antes que el de minería de datos) y en otras siendo descrito como un proceso dentro del descubrimiento de información, encargado de obtener conocimiento mediante la aplicación de algoritmos sobre datos recibidos~\cite{DataMiningConcepts}. 

A lo largo de esta memoria trataremos a la minería de datos como sinónimo de KDD, esto es, el conjunto de procesos que comprenden desde el pre procesamiento de los datos hasta la obtención y presentación de la información útil que contienen.

\section{Big Data}\label{sec:defBigData}

Big Data, pese a ser un término cuya definición no ha sido nunca estandarizada, podría definirse como un conjunto de datos tan grande y complejo con el que resulta imposible trabajar utilizando metodologías tradicionales \cite{bigDataDef}.

Fue utilizado por primera vez en 1997, en referencia a un problema donde un conjunto de datos tenía dificultad para ser almacenado en memoria o incluso en disco \cite{forbesBigData,cox1997application}. Este aspecto es vital para la comprensión y el trabajo en el área del Big Data: hemos de considerar, desde un primer momento, que no todo nuestros datos van a poder ser guardados en memoria al mismo tiempo.

Sin embargo, el volumen de información no es el único aspecto importante a considerar cuando tratamos con el Big Data. Existe una expresión denominada ``Las cuatro uves del Big Data'' (\textit{The Four V's of Big Data}) que pretende definir las cuatro características básicas de este término: volumen, variedad, velocidad y veracidad \cite{IBMBigData4Vs}. Cualquier trabajo relacionado con el Big Data deberá tener en cuenta la influencia, en mayor o menor medida según el área de trabajo, de cada uno de estos términos y actuar en consecuencia.

En lo que se refiere a este proyecto, el término tiene una importancia fundamental: nuestro objetivo final es el de implementar nuevas soluciones que permitan trabajar sobre una gran cantidad de datos, haciendo frente a todos los problemas que esto pudiese suponer.

\section{Algoritmos de selección de instancias}\label{sec:DefAlgSel}

El objetivo de estos algoritmos es solucionar dos problemas que afectan a la minería de datos: la cantidad cada vez mayor de datos, y su calidad. Podremos, por lo tanto,  definirlos como una herramienta para eliminar, de un conjunto de instancias, aquellas que conocemos, o sospechamos, son superfluas o perjudiciales~\cite{IntroInstanceSelect}.

Suprimiendo una porción del conjunto de instancias durante la fase de pre procesamiento de los datos conseguimos que el tiempo de ejecución algoritmos posteriores se reduzca, dado que hay menos instancias a examinar, mientras que es posible llegar a mejorar los resultados obtenidos al finalizar el proceso de minería si conseguimos eliminar instancias ruidosas~\cite{IntroInstanceSelect}.

\subsection{Locality sensitive hashing instance selection (LSHIS)}\label{sec:defLSHIS}

LSH, no confundir con el algoritmo de selección de instancias que definiremos a continuación, es un algoritmo que permite identificar y agrupar elementos muy semejantes. Su comportamiento se basa en un conjunto de funciones \textit{hash} que tienen la característica fundamental la capacidad de asignar elementos similares a un mismo grupo (\textit{bucket}) con una alta probabilidad~\cite{LSHISPaper}.

El algoritmo LSHIS es un método de selección de instancias que se apoya en el uso de LSH. La idea es aplicar, sobre las instancias iniciales, un conjunto de funciones hash que permitan agrupar en un mismo bucket aquellas instancias con un alto grado de similitud. Posteriormente, y realizando este proceso durante varias iteraciones si es preciso, de cada uno de esos buckets seleccionaremos una instancia de cada clase para formar el conjunto de instancias final. 

La ventaja de este algoritmo frente a otras alternativas es que permite realizar la selección de instancias en un tiempo de complejidad lineal, en comparación con soluciones de complejidad cuadrática o logarítmica~\cite{LSHISPaper}.


%LSHIS-PSEUDOCÓDIGO DEL PAPER
\begin{algorithm*}
\DontPrintSemicolon
\KwIn{Conjunto de instancias $ X = \lbrace(\mathbf{x}_{1},y_{1}),...,(\mathbf{x}_{n},y_{n})\rbrace$,
      conjunto $\mathcal{G}$ de familias de funciones hash}
\KwOut{Conjunto de instancias seleccionado $ S \subset X $ }

$ S = \varnothing $

\ForEach {instancia $ \mathbf{x} \in X $} {
  \ForEach {familia de funciones $g \in\mathcal{G} $} {
     $u\leftarrow$ cubeta asignada por la familia $g$ a la instancia $\mathbf{x}$ \;
     \If {no existen otras instancias de la misma clase que $\mathbf{x}$ en $u$}
     {
       Añadir $ \mathbf{x} $ a $ S $ \;
       Añadir $ \mathbf{x} $ a $ u $ \;
     }
  }
}

\Return {$S$}
\caption{LSH-IS -- Algoritmo de selección de instancias mediante hashing. \cite{LSHISPaper}}
\label{alg:LSHIS}
\end{algorithm*}

\subsection{Democratic Instance Selection}\label{sec:defDemoIS}

El algoritmo DemoIS es un método de selección de instancias que consiste en aplicar, durante un número variable de rondas, otros algoritmos de selección de instancias sobre subconjuntos disjuntos del gran conjunto inicial. En cada iteración, este algoritmo asignará unos ``votos'' a las instancias dependiendo de si han sido seleccionadas por el algoritmo al que se han sometido. Concluidas las rondas de votación, se realizará un cálculo con una función de \textit{fitness} para seleccionar todas aquellas instancias cuyos votos no hayan superado un determinado límite \cite{DemoISPaper}.
 
Al igual que el algoritmo LSHIS mencionado anteriormente, la motivación fundamental es la de crear un algoritmo que requiera una carga computacional menor que las soluciones tradicionales. En este caso, eso se consigue mediante la división del conjunto de datos original en otros más pequeños y, por supuesto, el correcto análisis de los resultados anteriores.


%LSHIS-PSEUDOCÓDIGO DEL PAPER
\begin{algorithm*}
\DontPrintSemicolon
\KwIn{Conjunto de instancias $ T = \lbrace(\mathbf{x}_{1},y_{1}),...,(\mathbf{x}_{n},y_{n})\rbrace$,
      tamaño de los subconjuntos $s$,
      número de rondas $r$}
\KwOut{Conjunto de instancias seleccionado $ S \subset T $ }

\For {$k = 1$ to $r$}{

  Dividimos las instancias en subjuntos disjuntos $t_j$ de tamaño $s$ tal que $\bigcup_i t_j = T$
  
  \For {$j = 1$ to $s$} {
  
  	Aplicamos un algoritmo de selección de instancias a $t_j$
  	
  	Añadimos un voto a las instancias removidas de $t_j$
  	
  }

}
Calculamos la mejor función fitness en base a los votos

$v$ = Votos que producen la mejor función fitness

$S = T$

Eliminamos de S todas las instancias cuyo número de votos sea $\geq v$

\Return {$S$}
\caption{LSH-IS -- Algoritmo de selección de instancias Democratic instance selection. \cite{DemoISPaper}}
\label{alg:DemoIS}
\end{algorithm*}


\section{Computación paralela}\label{sec:CompParalela}

La computación paralela es un tipo de computación en la que múltiples operaciones son llevadas a cabo simultáneamente \cite{Almasi:1989}. Parte del principio de que algunos problemas pueden subdividirse en problemas independientes más pequeños que pueden resolverse al mismo tiempo.

Es un paradigma que desde el principio se usó para operaciones que requiriesen una gran carga computacional, pero que ha despertado mucho interés en los últimos años gracias a la fácil escalabilidad que puede ofrecer frente a otras alternativas, como el aumento de la frecuencia de los procesadores, que han alcanzado un punto donde resulta más difícil avanzar \cite{CompParalelaWiki}.

En lo que se refiere al uso de la memoria por parte de un sistema paralelo, existe la posibilidad de que la memoria sea compartida o distribuida dependiendo de si las unidades de procesamiento poseen un espacio de memoria común o cada unidad posee su propio espacio. En lo que se refiere a la tecnología que vamos a usar (ver sección \ref{sec:DefSpark}), la memoria siempre será distribuida~\cite{SparkPaper}.

\section{Escalabilidad}\label{sec:DefEscalabilidad}

Escalabilidad es la capacidad de un sistema para adaptarse y soportar una carga de trabajo cada vez mayor~\cite{Bondi:2000}.

Durante el desarrollo de la memoria siempre usaremos el término de escalabilidad para referirnos a la escalabilidad horizontal, aquella que consigue su objetivo añadiendo más nodos a un sistema.

También es una característica que puede aplicarse a los algoritmos, para los cuales definimos escalabilidad como la capacidad de funcionar eficientemente cuando se aplican sobre situaciones que requieran una gran carga de trabajo. En lo referente al proyecto, esa situación será, obviamente, tratar con grandes conjuntos de datos.

\section{Rendimiento}\label{sec:Rendimiento}

Aunque el término puede dar lugar a numerosas definiciones, en lo que se refiere a nuestro proyecto, nos referiremos al rendimiento como la velocidad a la que un algoritmo puede ofrecer el resultado esperado.

Entiéndase que, en el contexto del trabajo, el objetivo ha sido siempre el de intentar mejorar los tiempos de ejecución de nuestras implementaciones con respecto a soluciones anteriores que seguían otro tipo de paradigmas (secuencial). Nunca se ha planteado la posibilidad de mejorar otros aspectos, dejando este término con la definición indicada anteriormente.


