\apendice{Especificación de diseño}

\section{Introducción}

En este documento se especificarán todas aquellas cuestiones que tengan relación con la manera en la que han organizado los componentes que forman el proyecto, así como la debida justificación de dicha organización.

\section{Diseño de datos}

Cuando en este proyecto hablamos de datos, es obligatorio referirse a los conjuntos de datos que estamos indicados a analizar. Es por ello que se ve necesario definir cuáles han de ser las características necesarias para que los mencionados conjuntos sean analizados correctamente.

A lo largo de esta sección vamos a tratar diferentes aspectos referentes al formato que han de cumplir los datos para que nuestro proyecto funcione sin problemas.

\subsection{Estructura de los conjuntos de datos}

En primer lugar, hemos de hablar del almacenamiento. En \textit{Big Data} es habitual contar con grandes conjuntos de datos almacenados en un sistema de ficheros distribuido al que tienen acceso una gran cantidad de nodos. En nuestro caso, el conjunto de datos inicial puede estar un  sistema de ficheros local (pero replicado en todos los nodos) o ser leído de un sistema de ficheros HDFS (Hadoop Distributed File System) o cualquier otro sistema de ficheros distribuidos soportado por Hadoop. A nivel local se ha trabajado con la primera opción, pues solo contamos con un único nodo. Cuando se ha utilizado la computación en la nube de Google Cloud Dataproc contábamos con el sistema de ficheros distribuidos que proporciona el servicio Google Cloud Storage.

En segundo lugar, es necesario mencionar el formato del fichero destinado a ser leído por el programa. Dicho fichero podrá contener una cabecera, que deberá ser eliminada por el programa, y un conjunto de instancias a razón de una instancia por línea. Los atributos de cada instancia estarán separados por comas, pudiendo estar el atributo de clase en primera o última posición.

\subsection{Preprocesamiento de los datos}

Spark, y en particular MLlib, es una librería relativamente moderna que todavía no proporciona soporte para muchos tipos de operaciones. En lo que a este proyecto se refiere, se ha notado la falta de posibilidades para preprocesar los datos de entrada que utilizamos para nuestros algoritmos.

Dado que implementar este tipo de funcionalidades podría suponer una carga de trabajo aún mayor, los conjuntos de datos utilizados necesitan cumplir algunos requisitos para ser correctamente tratados:

\begin{itemize}
\item El conjunto de datos ha de contener solamente atributos numéricos, y esto incluye el atributo de clase.
\item El conjunto de datos debe haber sido normalizado con anterioridad para una correcta ejecución.
\item No han de existir atributos nulos.
\end{itemize}

\section{Diseño procedimental}

Esta sección estará dedicada a definir y detallar el flujo general de nuestro programa.

No se va a dedicar especial atención al funcionamiento de los algoritmos de selección de instancias implementados (LSHIS y DemoIS). La razón a esto sus que su pseudocódigos, junto con un esquema de su funcionamiento en paralelo, ya fueron mostrados en la memoria principal del trabajo.

\subsection{Diagrama de flujo de una ejecución}

En la imagen \ref{fig:img/anexo/diagrama_flujo} podemos ver el diagrama de flujo que define cómo se gestiona el lanzamiento de una tarea de minería. Independientemente del método seguido para definir un lanzamiento, el diagrama va a ser siempre el mismo, simplemente hay diferentes maneras de llegar al mismo primer punto de inicio (consola o interfaz).

\imagen{img/anexo/diagrama_flujo}{Diagrama del flujo principal del programa.}

Merece la pena recordar dos aspectos de lo que se habló en los aspectos relevantes de la memoria y que tienen influencia en el diagrama de flujo mostrado:
\begin{itemize}
\item Spark utiliza el paradigma de la ``llamada por necesidad'' o \textit{lazy evaluation}. Esto significa que no todas las operaciones se realizarán tal y como el código especifica, sino que algunas operaciones no serán ejecutadas hasta que sea estrictamente necesario hacerlo.
\item Spark no almacena ninguna estructura RDD en ningún nivel de memoria, si no se indica explícitamente en el código. Esto puede dar lugar a que algunos datos deban recalcularse si no se persistieron en su momento o si, pese a ser persistidos, fueron parcial o totalmente borrados por falta de espacio. Igualmente, aunque indiquemos que deseamos persistir los datos hay múltiples maneras de hacerlo y múltiples escenarios posibles según qué opción. Como este aspecto depende fuertemente del tipo de persistencia configurado en Spark, del conjunto de datos o hasta del algoritmo usado, no se menciona explícitamente en el diagrama \ref{fig:img/anexo/diagrama_flujo}.
\item Es necesario indicar que existen actualmente dos modos de lanzamiento, ambos realizando la misma función. Su única diferencia es que uno de los dos modos realiza la medición del tiempo del filtrado, lo que le obliga a ejecutar una serie de operaciones enfocadas de manera ligeramente diferente y algunas otras operaciones ``vacías'' (con una carga de trabajo ínfima y sin propósito ninguno más que el de forzar la ejecución del programa hasta cierto punto). Este aspecto tampoco se recoge en el diagrama de flujo, pero puede ser consultado en la sección \ref{subsec:ISClassExecTest} si se desea obtener más información.
\end{itemize}

\section{Diseño arquitectónico}
A lo largo de esta sección describiremos la estructura interna de nuestro programa, así como la comunicación o dependencia entre diferentes elementos.

\subsection{Paquetes}

\imagen{img/anexo/diagrama_de_paquetes}{Diagrama de paquetes.}

\begin{itemize}
\item \textbf{gui:} Contiene todas aquellas clases relacionadas con la interfaz gráfica. 
\item \textbf{launcher:} Paquete que almacena todas las clases relacionadas con el lanzamiento de las ejecuciones. Por su propia finalidad, requiere estar relacionada con el paquete \textit{utils}, \textit{instanceSelection} y \textit{classification}.
\item \textbf{instanceSelection:} Contiene las implementaciones de algoritmos de selección de instancias. Uno de sus subpaquetes proporciona un espacio donde alojar aquellos algoritmos que funcionen de manera secuencial.
\item \textbf{classification:} Contiene las implementaciones de los diferentes clasificadores. De nuevo, contiene un paquete donde poder almacenar algoritmos secuenciales.
\end{itemize}

\subsection{Diagrama de clases}

En la imagen \ref{fig:img/anexo/clases_diagrama} podemos observar cómo se relacionan todas las clases que intervienen en la ejecución de las tareas de minería. Esto implica que aquellas clases relacionadas con la interfaz gráfica han sido omitidas.

\figuraApaisadaSinMarco{0.8}{img/anexo/clases_diagrama}{Diagrama de clases de la aplicación.}{fig:img/anexo/clases_diagrama}{}


\subsection{Patrones de diseño}

A continuación se van a analizar algunos patrones de diseño que pueden encontrarse en las clases que intervienen en lanzamiento de las tareas de minería.

\textbf{Patrón de diseño Singleton}

En el lenguaje de programación Scala existen dos tipos de clases, aquellas definidas mediante la palabra reservada ``class'', que funcionan como, por ejemplo, en el lenguaje Java, y aquellas definidas mediante la palabra reservada ``object''.

Estas clases ``object'' de Scala corresponden a un tipo especial de clase con una serie de restricciones que la hacen actuar como un \textit{singleton}: No puede haber más de una instancia por clase, su constructor no puede tomar parámetros y la clase es accesible para cualquier otro elemento del programa del programa.

No existe intención de definir el funcionamiento del lenguaje de programación Scala, pero es necesario saber que cualquier clase que contenga una método \textit{main} o extienda de \textit{scala.App}, deberá ser una clase de tipo ``object''.

Por lo dicho, encontramos este patrón de diseño en las clases dedicadas a iniciar la ejecución: launcher.ExperimentLauncher y gui.SparkISGUI.


\textbf{Patrón de diseño Estrategia}

La intención de aplicar este patrón es poder seleccionar, en tiempo de ejecución, una determinada manera de realizar una acción (estrategia).

Se puede apreciar este patrón de diseño en las clases relacionadas con el lanzamiento de una ejecución, donde el usuario puede haber indicado por parámetro qué estrategia desea utilizar (ver diagrama en \ref{fig:img/anexo/estrategia_lanzador}). Como puede deducirse del diagrama, actualmente existen dos posibles estrategias sobre las que elegir, aunque si la librería continuase creciendo podrían implementarse tantas estrategias como sean necesarias. 

\imagen{img/anexo/estrategia_lanzador}{Diagrama de clases mostrando el patrón de diseño Estrategia.}

Del mismo modo, y con la misma finalidad, podremos encontrar un patrón de diseño estrategia entre las clases que realizan las labores de minería de datos y los algoritmos de selección de instancias y clasificación, tal y como muestra la imagen \ref{fig:img/anexo/estrategia_IS_y_Class}. Esto nos posibilita, de nuevo, poder seleccionar un tipo de algoritmo en tiempo de ejecución. En este caso, el diagrama solo muestra aquellos métodos que tienen relación con el patrón concreto, por considerarse este diagrama con más elementos que el anterior.

\figuraApaisadaSinMarco{1}{img/anexo/estrategia_IS_y_Class}{Diagrama de clases mostrando dos patrón de diseño Estrategia.}{fig:img/anexo/estrategia_IS_y_Class}{}

\subsection{Serialización únicamente lo necesario}

Uno de los problemas de diseño a los que nos hemos enfrentado durante la fase de implementación es el hecho de distribuir algunas clases por nuestra red de nodos.

Por norma general, hemos intentado serializar la menor cantidad de objetos posibles, algo que ha tenido una influencia en el diseño de los dos algoritmos de selección implementados: LSHIS y DemoIS. Podemos ver el diagrama de clases de los paquetes \textit{instanceSelection.lshis} y \textit{instanceSelection.demoIS} en las figuras \ref{fig:img/anexo/clases_LSHIS} y \ref{fig:img/anexo/clases_DemoIS} respectivamente.

Puede apreciarse, en los mencionados diagramas, que existe una clase principal no serializable que contiene el algoritmo, y una serie de clases anexas que heredan de la interfaz \textit{scala.Serializable}. Estas últimas clases serán las que contendrán información que verdaderamente queremos distribuir. De esta manera, conseguimos dividir aquellos elementos que deseamos mantener en una única máquina de todos aquellos que son necesarios en la red de nodos para realizar las operaciones pertinentes.

\figuraApaisadaSinMarco{0.8}{img/anexo/clases_LSHIS}{Diagrama de clases del paquete \textit{instanceSelection.lshis}.}{fig:img/anexo/clases_LSHIS}{}

\imagen{img/anexo/clases_DemoIS}{Diagrama de clases del paquete \textit{instanceSelection.demoIS}.}

